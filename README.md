# url-parser

A java bean for URLs of the http protocol. 

The class cat.inspiracio.url.URL is similar to java.net.URL,
but I prefer it for these reasons:

* Has getters and setters for Java bean style.
* Also has methods for fluid style.
* Is not final, so you can extend it.
* This class is only about parsing and formatting, not about networking.
* Implements https://url.spec.whatwg.org/, or at least that is the intention.

Usage:

    	URL u=new URL();
    	u.scheme("http").host("www.google.com").path("search");
    	assertEquals("http://www.google.com/search", u.toString());
    	u.parameter("q", "pine");
    	search(u.parameter("q"));
    	u.parameter("q", "cactus");
    	search(u.parameter("q"));
    	URL v=new URL(u.toString());

