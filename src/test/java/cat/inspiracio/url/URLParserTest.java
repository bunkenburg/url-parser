/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class URLParserTest {

    @Before public void setUp(){}
    
    @After public void tearDown(){}
    
    @Test public void tp0(){p("path", null, null, null, 0, "path", null);}
    @Test public void tp1(){p("/path", null, null, null, 0, "/path", null);}
    @Test public void tp2(){p("http://www.google.com/path", "http", null, "www.google.com", 80, "/path", null);}
    @Test public void tp3(){p("//www.google.com:80#anchor", null, null, "www.google.com", 80, null, "anchor");}
    @Test public void tp4(){p("http://alex@www.google.com", "http", "alex", "www.google.com", 80, null, null);}
    
    //toString() gives: "file:/Users/alex/Documents/bla.pdf" which equals "file:///Users/alex/Documents/bla.pdf".
    @Test public void tp5(){p("file:///Users/alex/Documents/bla.pdf", "file", null, null, 0, "/Users/alex/Documents/bla.pdf", null);}
    
    void p(String u, String scheme, String user, String server, int port, String path, String fragment){
        URL url=new URL(u);
        assertEquals(scheme, url.scheme());
        assertEquals(user, url.username());
        assertEquals(server, url.server());
        assertEquals(port, url.port());
        assertEquals(path, url.path());
        assertEquals(fragment, url.fragment());
    }

}
