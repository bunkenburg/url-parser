/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class URLParametersTest {

	@Before public void setUp(){}
	
	@After public void tearDown(){}

	@Test public void ttoStringMultiples(){
		URLParameters parameters=new URLParameters().add("name", "Alex").add("name", "Bunk");
		String s=parameters.toString();
		assertEquals(s, "name=Alex&name=Bunk");
	}

	@Test public void tEncoding(){
		URLParameters parameters=new URLParameters().add("name", "Alex Bunk");
		String s=parameters.toString();
		assertEquals(s, "name=Alex+Bunk");
	}

	@Test public void tDecoding(){
		URLParameters parameters=new URLParameters("name=Alex+Bunk");
		String s=parameters.get("name");
		assertEquals(s, "Alex Bunk");
	}

	/** For equality, order of parameters is irrelevant. */
	@Test public void tEquals(){
		URLParameters ps=new URLParameters().add("a", "Aardvark").add("a", "Advocate").add("b", "Shortcake");
		URLParameters qs=new URLParameters().add("b", "Shortcake").add("a", "Advocate").add("a", "Aardvark");
		boolean b=ps.equals(qs);
		assertTrue(b);
	}

}
