/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class URLTest {

	@Before public void setUp(){}
	
	@After public void tearDown(){}
	
    @Test public void tSpecial0(){special("ftp://www.google.com", true);}
    @Test public void tSpecial1(){special("file://www.google.com", true);}
    @Test public void tSpecial2(){special("gopher://www.google.com", true);}
    @Test public void tSpecial3(){special("http://www.google.com", true);}
    @Test public void tSpecial4(){special("https://www.google.com", true);}
    @Test public void tSpecial5(){special("ws://www.google.com", true);}
    @Test public void tSpecial6(){special("wss://www.google.com", true);}
    @Test public void tSpecial7(){special("wap://www.google.com", false);}
    @Test public void tSpecial8(){special("//www.google.com", false);}
    void special(String u, boolean s){assertEquals(s, new URL(u).isSpecial());}

    @Test public void tLocal0(){local("about://www.google.com", true);}
    @Test public void tLocal1(){local("blob://www.google.com", true);}
    @Test public void tLocal2(){local("data://www.google.com", true);}
    @Test public void tLocal3(){local("filesystem://www.google.com", true);}
    @Test public void tLocal4(){local("https://www.google.com", false);}
    @Test public void tLocal5(){local("ws://www.google.com", false);}
    @Test public void tLocal6(){local("wss://www.google.com", false);}
    @Test public void tLocal7(){local("wap://www.google.com", false);}
    @Test public void tLocal8(){local("//www.google.com", false);}
    @Test public void tLocal9(){local("file://www.google.com", false);}
    void local(String u, boolean s){assertEquals(s, new URL(u).isLocal());}

    @Test public void tDefaultPort0(){defaultPort(null, 0);}
    @Test public void tDefaultPort1(){defaultPort("ftp", 21);}
    @Test public void tDefaultPort2(){defaultPort("file", 0);}
    @Test public void tDefaultPort3(){defaultPort("gopher", 70);}
    @Test public void tDefaultPort4(){defaultPort("http", 80);}
    @Test public void tDefaultPort5(){defaultPort("https", 443);}
    @Test public void tDefaultPort6(){defaultPort("ws", 80);}
    @Test public void tDefaultPort7(){defaultPort("wss", 443);}
    @Test public void tDefaultPort8(){defaultPort("bla", 0);}
	void defaultPort(String scheme, int p){assertEquals(p, URL.getDefaultPort(scheme));}
	
    @Test public void tAbsolute0(){absolute("about://www.google.com", true);}
    @Test public void tAbsolute1(){absolute("blob://www.google.com", true);}
    @Test public void tAbsolute2(){absolute("data://www.google.com", true);}
    @Test public void tAbsolute3(){absolute("file://www.google.com", true);}
    @Test public void tAbsolute4(){absolute("https://www.google.com", true);}
    @Test public void tAbsolute5(){absolute("ws://www.google.com", true);}
    @Test public void tAbsolute6(){absolute("wss://www.google.com", true);}
    @Test public void tAbsolute7(){absolute("wap://www.google.com", true);}
    @Test public void tAbsolute8(){absolute("//www.google.com", false);}
    @Test public void tAbsolute9(){absolute("ABOUT://www.google.com", true);}
    @Test public void tAbsolute10(){absolute("blob://www.google.com", true);}
    @Test public void tAbsolute11(){absolute("data://www.google.com", true);}
    @Test public void tAbsolute12(){absolute("file://www.google.com", true);}
    @Test public void tAbsolute13(){absolute("https://www.google.com", true);}
    @Test public void tAbsolute14(){absolute("ws://www.google.com", true);}
    @Test public void tAbsolute15(){absolute("wss://www.google.com", true);}
    @Test public void tAbsolute16(){absolute("HTTP://www.google.com", true);}
    @Test public void tAbsolute17(){absolute("/bla.html", false);}
    void absolute(String u, boolean a){assertEquals(a, new URL(u).isAbsolute());}

    @Test public void tRelative0(){relative("//www.google.com", true);}
    @Test public void tRelative1(){relative("blob://www.google.com", false);}
    @Test public void tRelative2(){relative("/bla/di", true);}
    @Test public void tRelative3(){relative("di", true);}
    void relative(String u, boolean r){assertEquals(r, new URL(u).isRelative());}

    @Test public void tScheme0(){scheme("http://www.google.com", "http");}
    @Test public void tScheme1(){scheme("https://www.google.com", "https");}
    @Test public void tScheme2(){scheme("//www.google.com", null);}
    @Test public void tScheme3(){scheme("/absolute", null);}
    @Test public void tScheme4(){scheme("relative", null);}
    @Test public void tScheme5(){scheme("?b=bla", null);}
    @Test public void tScheme6(){scheme("#anchor", null);}
    @Test public void tScheme7(){scheme("", null);}
    void scheme(String url, String scheme){assertEquals(scheme, new URL(url).scheme());}

    //The spec for pop() is not so precise. Instability here.
    @Test public void tPop0(){pop("http://www.google.com", "http://www.google.com");}
    @Test public void tPop1(){pop("https://www.google.com/bla", "https://www.google.com/");}
    @Test public void tPop2(){pop("//www.google.com/bla/di", "//www.google.com/bla/");}
    @Test public void tPop3(){pop("/absolute", "/");}
    @Test public void tPop4(){pop("relative", "");}
    @Test public void tPop5(){pop("?b=bla", "");}
    @Test public void tPop6(){pop("#anchor", "");}
    @Test public void tPop7(){pop("file://bla/di", "file://bla/");}
    @Test public void tPop8(){pop("file://E:bla/di", "file://E:bla/");}
    @Test public void tPop10(){pop("http://www.yellow.cat/stories/alex/bla/bla.html#anchor", "http://www.yellow.cat/stories/alex/bla/");}
    @Test public void tPop11(){pop("http://www.yellow.cat/stories/alex/bla/bla.html", "http://www.yellow.cat/stories/alex/bla/");}
    void pop(String url, String popped){assertEquals(popped, new URL(url).pop().toString());}
    
    @Test public void tServer0(){server("http://www.google.com", "www.google.com");}
    @Test public void tServer1(){server("https://www.google.com", "www.google.com");}
    @Test public void tServer2(){server("//www.google.com", "www.google.com");}
    @Test public void tServer3(){server("http://www.google.com?b=bla", "www.google.com");}
    @Test public void tServer4(){server("http://www.google.com/path", "www.google.com");}
    @Test public void tServer5(){server("http://www.google.com#anchor", "www.google.com");}
    @Test public void tServer6(){server("http://www.google.com:80", "www.google.com");}
    @Test public void tServer7(){server("http://localhost:80", "localhost");}
    @Test public void tServer8(){server("http://127.0.0.1:80", "127.0.0.1");}
    @Test public void tServer9(){server("/absolute", null);}
    @Test public void tServer10(){server("relative", null);}
    @Test public void tServer11(){server("?b=bla", null);}
    @Test public void tServer12(){server("#anchor", null);}
    @Test public void tServer13(){server("", null);}
    void server(String url, String server){assertEquals(server, new URL(url).server());}
    
    @Test public void tHostname0(){hostname("http://www.google.com", "www.google.com");}
    @Test public void tHostname1(){hostname("https://www.google.com", "www.google.com");}
    @Test public void tHostname2(){hostname("//www.google.com", "www.google.com");}
    @Test public void tHostname3(){hostname("http://www.google.com?b=bla", "www.google.com");}
    @Test public void tHostname4(){hostname("http://www.google.com/path", "www.google.com");}
    @Test public void tHostname5(){hostname("http://www.google.com#anchor", "www.google.com");}
    @Test public void tHostname6(){hostname("http://www.google.com:80", "www.google.com");}
    @Test public void tHostname7(){hostname("http://localhost:80", "localhost");}
    @Test public void tHostname8(){hostname("http://127.0.0.1:80", "127.0.0.1");}
    @Test public void tHostname9(){hostname("/absolute", null);}
    @Test public void tHostname10(){hostname("relative", null);}
    @Test public void tHostname11(){hostname("?b=bla", null);}
    @Test public void tHostname12(){hostname("#anchor", null);}
    @Test public void tHostname13(){hostname("", null);}
    void hostname(String url, String host){assertEquals(host, new URL(url).getHostname());}
    
    @Test public void tPort0(){port("http://www.google.com?b=bla", 80);}
    @Test public void tPort1(){port("http://www.google.com/path", 80);}
    @Test public void tPort2(){port("http://www.google.com#anchor", 80);}
    @Test public void tPort3(){port("http://www.google.com:80", 80);}
    @Test public void tPort4(){port("http://www.google.com:80?b=bla", 80);}
    @Test public void tPort5(){port("http://www.google.com:80/path", 80);}
    @Test public void tPort6(){port("http://www.google.com:80#anchor", 80);}
    @Test public void tPort7(){port("https://www.google.com?b=bla", 443);}
    @Test public void tPort8(){port("https://www.google.com/path", 443);}
    @Test public void tPort9(){port("https://www.google.com#anchor", 443);}
    @Test public void tPort10(){port("https://www.google.com:443", 443);}
    @Test public void tPort11(){port("https://www.google.com:443?b=bla", 443);}
    @Test public void tPort12(){port("https://www.google.com:443/path", 443);}
    @Test public void tPort13(){port("https://www.google.com:443#anchor", 443);}
    @Test public void tPort14(){port("//www.google.com:80#anchor", 80);}
    @Test public void tPort15(){port("//www.google.com:443#anchor", 443);}
    @Test public void tPort16(){port("https://www.google.com:8888#anchor", 8888);}
    @Test public void tPort17(){port("//www.google.com:8888#anchor", 8888);}
    @Test public void tPort18(){port("//www.google.com:8888#anchor", 8888);}
    void port(String url, int port){assertEquals(port, new URL(url).port());}
    
    @Test public void tHost0(){host("http://www.google.com?b=bla", "www.google.com");}
    @Test public void tHost1(){host("http://www.google.com/path", "www.google.com");}
    @Test public void tHost2(){host("http://www.google.com#anchor", "www.google.com");}
    @Test public void tHost3(){host("http://www.google.com:80", "www.google.com");}
    @Test public void tHost4(){host("http://www.google.com:80?b=bla", "www.google.com");}
    @Test public void tHost5(){host("http://www.google.com:80/path", "www.google.com");}
    @Test public void tHost6(){host("http://www.google.com:80#anchor", "www.google.com");}
    @Test public void tHost7(){host("https://www.google.com?b=bla", "www.google.com");}
    @Test public void tHost8(){host("https://www.google.com/path", "www.google.com");}
    @Test public void tHost9(){host("https://www.google.com#anchor", "www.google.com");}
    @Test public void tHost10(){host("https://www.google.com:443", "www.google.com");}
    @Test public void tHost11(){host("https://www.google.com:443?b=bla", "www.google.com");}
    @Test public void tHost12(){host("https://www.google.com:443/path", "www.google.com");}
    @Test public void tHost13(){host("https://www.google.com:443#anchor", "www.google.com");}
    @Test public void tHost14(){host("//www.google.com:80#anchor", "www.google.com:80");}
    @Test public void tHost15(){host("//www.google.com:443#anchor", "www.google.com:443");}
    @Test public void tHost16(){host("http://www.google.com:8888#anchor", "www.google.com:8888");}
    @Test public void tHost17(){host("https://www.google.com:8888#anchor", "www.google.com:8888");}
    @Test public void tHost18(){host("//www.google.com:8888#anchor", "www.google.com:8888");}
    void host(String url, String host){assertEquals(host, new URL(url).getHost());}
    
    //Username is UTF8-% encoded. ä == %C3%A4
    @Test public void tUsername0(){user("http://www.google.com?b=bla", null);}
    @Test public void tUsername1(){user("http://alex@www.google.com/path", "alex");}
    @Test public void tUsername2(){user("http://@www.google.com/path", "");}
    @Test public void tUsername3(){user("http://alex:pass@www.google.com/path", "alex");}
    @Test public void tUsername4(){user("http://alex:@www.google.com/path", "alex");}
    @Test public void tUsername7(){user("https://www.google.com?b=bla", null);}
    @Test public void tUsername8(){user("https://alex@www.google.com/path", "alex");}
    @Test public void tUsername9(){user("https://:@www.google.com/path", "");}
    @Test public void tUsername10(){user("https://%C3%A4@www.google.com/path", "ä");}
    @Test public void tUsername14(){user("//www.google.com:80#anchor", null);}
    @Test public void tUsername15(){user("//alex@www.google.com:443#anchor", "alex");}
    void user(String u, String user){assertEquals(user, new URL(u).username());}
    
    //Password is UTF8-% encoded. ä == %C3%A4
    @Test public void tPassword0(){pw("http://www.google.com?b=bla", null);}
    @Test public void tPassword1(){pw("http://alex@www.google.com/path", null);}
    @Test public void tPassword2(){pw("http://alex:pass@www.google.com/path", "pass");}
    @Test public void tPassword3(){pw("http://alex:@www.google.com/path", "");}
    @Test public void tPassword4(){pw("http://:@www.google.com/path", "");}
    @Test public void tPassword5(){pw("http://:pass@www.google.com/path", "pass");}
    @Test public void tPassword6(){pw("https://alex:%C3%A4@www.google.com/path", "ä");}
    void pw(String u, String pw){assertEquals(pw, new URL(u).password());}
    
	@Test public void tPortStandard0(){portStandard("http://www.google.com?b=bla", true);}
	@Test public void tPortStandard1(){portStandard("https://www.google.com?b=bla", true);}
	@Test public void tPortStandard2(){portStandard("http://www.google.com:80?b=bla", true);}
	@Test public void tPortStandard3(){portStandard("https://www.google.com:443?b=bla", true);}
	@Test public void tPortStandard4(){portStandard("//www.google.com?b=bla", true);}
	@Test public void tPortStandard6(){portStandard("http://www.google.com:80", true);}
	@Test public void tPortStandard7(){portStandard("https://www.google.com:443", true);}
    @Test public void tPortStandard8(){portStandard("//www.google.com:80", false);}
    @Test public void tPortStandard9(){portStandard("http://www.google.com:81?b=bla", false);}
    @Test public void tPortStandard10(){portStandard("https://www.google.com:444?b=bla", false);}
    @Test public void tPortStandard11(){portStandard("http://www.google.com:81?b=bla", false);}
    @Test public void tPortStandard12(){portStandard("https://www.google.com:444?b=bla", false);}
    @Test public void tPortStandard13(){portStandard("//www.google.com:555", false);}
	void portStandard(String u, boolean b){assertEquals(b, new URL(u).isDefaultPort());}
	
    @Test public void tOrigin0(){origin("http://www.google.com:1234", "http://www.google.com:1234");}
    @Test public void tOrigin1(){origin("http://www.google.com:8888", "http://www.google.com:8888");}
	void origin(String url, String origin){assertEquals(origin, new URL(url).getOrigin());}
	
	@Test public void tPath0(){path("/path", "/path");}
	@Test public void tPath1(){path("//www.google.com/path", "/path");}
	@Test public void tPath2(){path("//www.google.com:80/path", "/path");}
	@Test public void tPath3(){path("/path#anchor", "/path");}
	@Test public void tPath4(){path("/path?b=bla", "/path");}
	@Test public void tPath5(){path("relative?b=bla", "relative");}
	void path(String url, String path){assertEquals(path, new URL(url).path());}

	@Test public void tExtension0(){extension("http://www.google.com/path.html", "html");}
	@Test public void tExtension1(){extension("http://www.google.com/path.jpg", "jpg");}
	@Test public void tExtension2(){extension("http://www.google.com/path", null);}
	@Test public void tExtension3(){extension("http://www.google.com/path.html?b=bla", "html");}
	@Test public void tExtension4(){extension("http://www.google.com/path.html#anchor", "html");}
	@Test public void tExtension5(){extension("http://www.google.com/path/", null);}
	@Test public void tExtension6(){extension("http://www.google.com:8888", null);}
	void extension(String url, String extension){assertEquals(extension, new URL(url).extension());}
	
    //user and password are UTF8-% encoded. ä == %C3%A4
	@Test public void tParameter0(){param("", "a", null);}
	@Test public void tParameter1(){param("?", "a", null);}
	@Test public void tParameter2(){param("path", "a", null);}
	@Test public void tParameter3(){param("path?", "a", null);}
	@Test public void tParameter4(){param("#anchor", "a", null);}
	@Test public void tParameter5(){param("?a=bla", "b", null);}
	@Test public void tParameter6(){param("?a", "a", "");}
	@Test public void tParameter7(){param("http://www.yellow.cat/stories/space/script.html?m=next", "m", "next");}
    @Test public void tParameter8(){param("?a=a&a=b", "a", "a");}
    @Test public void tParameter9(){param("?a=a&b=b&c=c", "a", "a");}
    @Test public void tParameter10(){param("?a=a&b=b&c=c#fragment", "a", "a");}
    @Test public void tParameter11(){param("?a=%C3%A4", "a", "ä");}
	void param(String url, String key, String value){assertEquals(value, new URL(url).parameter(key));}
	
	@Test public void tParameters0(){params("?a=a&a=b", "a", "a", "b");}
	void params(String url, String key, String... value){
	    List<String>vs=Arrays.asList(value);
	    assertEquals(vs, new URL(url).parameters(key));
	}

    //Fragments are UTF8-% encoded. ä == %C3%A4
    @Test public void tFragment0(){frag("", null);}
    @Test public void tFragment1(){frag("?b=bla", null);}
    @Test public void tFragment2(){frag("path.html", null);}
    @Test public void tFragment3(){frag("#anchor", "anchor");}
    @Test public void tFragment4(){frag("path.html#anchor", "anchor");}
    @Test public void tFragment5(){frag("http://www.yellow.cat:8888/path.html#anchor", "anchor");}
    @Test public void tFragment6(){frag("?b=bla#fragment", "fragment");}
    @Test public void tFragment7(){frag("#", "");}
    @Test public void tFragment8(){frag("#%C3%A4", "ä");}
    void frag(String url, String anchor){assertEquals(anchor, new URL(url).fragment());}
    
    @Test public void tHash0(){hash("", "");}
    @Test public void tHash1(){hash("?b=bla", "");}
    @Test public void tHash2(){hash("path.html", "");}
    @Test public void tHash3(){hash("#anchor", "#anchor");}
    @Test public void tHash4(){hash("path.html#anchor", "#anchor");}
    @Test public void tHash5(){hash("http://www.yellow.cat:8888/path.html#anchor", "#anchor");}
    @Test public void tHash6(){hash("?b=bla#fragment", "#fragment");}
    @Test public void tHash7(){hash("#", "#");}
    void hash(String url, String h){assertEquals(h, new URL(url).getHash());}
    
    //URL equality takes into account default ports, blind to order of URL parameters.
	@Test public void tEqual0(){equal("http://www.google.com?a=aa&a=ab&b=b", "http://www.google.com?b=b&a=ab&a=aa");}
	@Test public void tEqual1(){equal("http://www.google.com:80", "http://www.google.com");}
	@Test public void tEqual2(){equal("https://www.google.com:443", "https://www.google.com");}
	void equal(String u0, String u1){assertEquals(new URL(u0), new URL(u1));}

	@Test public void tMime1(){mime("", null);}
	@Test public void tMime2(){mime("http://www.yellow.cat", null);}
	@Test public void tMime3(){mime("http://www.yellow.cat/file", null);}
	@Test public void tMime4(){mime("http://www.yellow.cat/file.jpg", "image/jpeg");}
	
	@Test public void tMime5(){mime("file.csv", "text/csv");}
	@Test public void tMime6(){mime("file.txt", "text/plain");}
	@Test public void tMime7(){mime("file.html?u=Opuntia", "text/html");}
	@Test public void tMime8(){mime("file.html#Opuntia", "text/html");}
    @Test public void tMime9(){mime("file.xml", "application/xml");}
    void mime(String url, String type){assertEquals(type, new URL(url).mimeType());}

    //user and password are UTF8-% encoded. ä == %C3%A4
    @Test public void tstring0(){
        URL u=new URL("http://google.com");
        u.username("ä");
        u.password("ä");
        assertEquals("http://%C3%A4:%C3%A4@google.com", u.toString());
    }
    //URL parameter keys and values are UTF8-% encoded. ä == %C3%A4
    @Test public void tstring1(){
        URL u=new URL("http://google.com");
        u.parameter("a", "ä");
        assertEquals("http://google.com?a=%C3%A4", u.toString());
    }
    @Test public void tstring2(){
        URL u=new URL("http://google.com");
        u.parameter("ä", "ä");
        assertEquals("http://google.com?%C3%A4=%C3%A4", u.toString());
    }
    //Fragments are UTF8-% encoded. ä == %C3%A4
    @Test public void tstring3(){
        URL u=new URL("http://google.com");
        u.fragment("ä");
        assertEquals("http://google.com#%C3%A4", u.toString());
    }
    
    @Test public void encode() throws UnsupportedEncodingException{
        String s="ä";
        String e=URLEncoder.encode(s, "UTF-8");
        assertEquals("%C3%A4", e);
        String d=URLDecoder.decode(e, "UTF-8");
        assertEquals(s, d);
    }

    @Test public void tpath0() throws UnsupportedEncodingException{
    	URL u=new URL();
    	u.scheme("http").host("www.google.com").path("search");
    	assertEquals("http://www.google.com/search", u.toString());
    	u.parameter("q", "pine");
    	assertEquals("pine", u.parameter("q"));
    	u.parameter("q", "cactus");
    	assertEquals("cactus", u.parameter("q"));
    }

    @Test public void tpath1() throws UnsupportedEncodingException{
    	URL u=new URL("http://www.google.com/search");
    	assertEquals("/search", u.path());
    }
}