/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

/** Interface URLUtils. 
 * 
 * <a href="http://www.w3.org/TR/url/#dom-urlutils">spec</a>
 * <a href="http://url.spec.whatwg.org/#urlutils">spec</a> 
 *  */
public interface URLUtils {
	
	String getHref();
	void setHref(String href);
	
	String getOrigin();
	
	String getProtocol();
	void setProtocol(String protocol);
	
	String getUsername();
	void setUsername(String u);

	String getPassword();
	void setPassword(String p);
	
	String getHost();
	void setHost(String host);
	
	String getHostname();
	void setHostname(String name);

	/** @return Gets port as int, or 0. Not in spec. */
    int port();
    String getPort();
    void setPort(String port);
    void setPort(int port);
	
	String getPathname();
	void setPathname(String path);
	
	String getSearch();
	void setSearch(String search);
	
	URLSearchParams getSearchParams();
	void setSearchParams(URLSearchParams params);
	
	String getHash();
	void setHash(String hash);
	
}
