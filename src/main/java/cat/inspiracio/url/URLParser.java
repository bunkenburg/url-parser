/*
Copyright 2015 Alexander Bunkenburg <alex@inspiracio.cat>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cat.inspiracio.url;

/** https://url.spec.whatwg.org/#url-parsing 
 * 
 * Differences from that spec: This parser can also parse relative URLs without a 
 * base URL. For example: 
 * 
 *  URL u = new URL("/path");
 * 
 * is okay.
 * */
class URLParser {
    
    URLParser(){}
    
    URL parse(String input){return parse(input, null);}
    
    URL parse(String input, URL base){return parse(input, base, null);}
    
    URL parse(String input, URL base, String encodingOverride){
        //1. Let url be the result of running the basic URL parser on input with 
        //base, and encoding override as provided.
        //2. If url is failure, return failure.
        URL url=basic(input, base, encodingOverride);//throws exception
        //3. If url’s scheme is not "blob", return url.
        if(!"blob".equals(url.scheme()))return url;
        //4. If the first string in url’s path is not in the blob URL store, return url. [FILEAPI]
        throw new UnsupportedOperationException();
        //5. Set url’s object to a structured clone of the entry in the blob URL 
        //store corresponding to the first string in url’s path. [HTML]
        //6. Return url.
    }
    
    /** https://url.spec.whatwg.org/#concept-basic-url-parser */
    URL basic(String input){return basic(input, null);}
    
    URL basic(String input, URL base){return basic(input, base, null);}
    
    /** returns either a new URL or failure */
    URL basic(String input, URL base, String encodingOverride){
        return basic(input, base, encodingOverride, null, null);
    }
    
    /** Modifies the passed url and can terminate without returning anything. */
    URL basic(String input, URL base, String encodingOverride, URL url, State stateOverride){
        Machine m=new Machine(input, base, encodingOverride, url, stateOverride);
        m.init();
        return m.run();
    }

}
